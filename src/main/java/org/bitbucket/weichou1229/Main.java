package org.bitbucket.weichou1229;

import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;

public class Main {

    public static void main(String[] args){
        if(args.length<3 || args[0]==null || args[1]==null || args[2]==null){
            System.out.println("Plz give 1.IP 2.Port 3.Starting address");
            System.out.println("Ex: java -jar j2mod-sample.jar 192.168.99.100 502 1");
            return;
        }

        String ip = args[0];
        int port = Integer.parseInt(args[1]);
        int startingAddress = Integer.parseInt(args[2]);

        System.out.println("> IP is " + ip);
        System.out.println("> Port is " + port);
        System.out.println("> Starting address is " + startingAddress);
        ModbusTCPMaster master = null ;
        try {
            master = new ModbusTCPMaster(ip,port);
            master.connect();

            System.out.println("===================================================");
            System.out.println("");
            System.out.println(" Read  ["+ip+":"+port+"] "+startingAddress+"-> "+ master.readMultipleRegisters(startingAddress, 1)[0].getValue());
            System.out.println("");
            System.out.println("===================================================");
        }
        catch (Exception e) {
            System.out.println("Cannot initialise tests ->"+ e.getMessage());
        }
        finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
}
